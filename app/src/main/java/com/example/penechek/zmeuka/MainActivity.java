package com.example.penechek.zmeuka;

//import android.support.v7.app.ActionBarActivity;
import android.graphics.Paint;
import android.graphics.Point;
import android.os.Bundle;
//import android.view.Menu;
//import android.view.MenuItem;

import android.app.Activity;
import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;

import java.util.Random;


public class MainActivity extends Activity {
    Random rand;
    Point zmei[];
    int ssize=20;
    float x,y;
    int nx,ny;
    int tek=50;
    int vect=1;
    DrawView DV;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rand=new Random();
        zmei=new Point[tek];

        for (int i=0; i<zmei.length; i++)
        {
            zmei[i]=new Point();
            zmei[i].x=i*ssize;
            zmei[i].y=0;
        }

        setContentView(new DrawView(this));
    }

    class DrawView extends SurfaceView implements SurfaceHolder.Callback,View.OnTouchListener {

        private DrawThread drawThread;

        public DrawView(Context context) {
            super(context);
            getHolder().addCallback(this);
            setOnTouchListener(this);
        }

        @Override
        public void surfaceChanged(SurfaceHolder holder, int format, int width,
                                   int height) {

        }

        @Override
        public void surfaceCreated(SurfaceHolder holder) {
            drawThread = new DrawThread(getHolder());
            drawThread.setRunning(true);
            drawThread.start();

        }

        @Override
        public void surfaceDestroyed(SurfaceHolder holder) {
            boolean retry = true;
            drawThread.setRunning(false);
            while (retry) {
                try {
                    drawThread.join();
                    retry = false;
                } catch (InterruptedException e) {
                }
            }
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            x = event.getX();
            y = event.getY();

            switch (event.getAction()){

                case MotionEvent.ACTION_DOWN:
                    if((y<getHeight()/4) && vect!=2) {
                        vect=4;

                    }
                    else if ((y>getHeight()/4*3) && vect!=4)
                    {
                        vect=2;
                    }
                    else if ((x<getWidth()/2) && vect!=1){
                        vect=3;
                    }
                    else if ((x>getWidth()/2) && vect!=3){
                        vect=1;
                    }

                    break;
            }
            return false;
        }

        class DrawThread extends Thread {
 Paint paint;
            int Width,Height;

            private boolean running = false;
            private SurfaceHolder surfaceHolder;

            public DrawThread(SurfaceHolder surfaceHolder) {
                paint=new Paint();
                this.surfaceHolder = surfaceHolder;
            }

            public void setRunning(boolean running) {
                this.running = running;
            }


            void upd()
            {
                switch(vect)
                {
                    case 1:
                        nx=zmei[tek-1].x+ssize;
                        ny=zmei[tek-1].y;
                        break;
                    case 2:
                        nx=zmei[tek-1].x;
                        ny=zmei[tek-1].y+ssize;
                        break;
                    case 3:
                        nx=zmei[tek-1].x-ssize;
                        ny=zmei[tek-1].y;
                        break;
                    case 4:
                        nx=zmei[tek-1].x;
                        ny=zmei[tek-1].y-ssize;
                        break;
                }
                movezmei(nx,ny);
            }
            void movezmei(int nx,int ny)
            {
                for (int i=0; i<(tek-1); i++)
                {
                    zmei[i].x=zmei[i+1].x;
                    zmei[i].y=zmei[i+1].y;
                }
                zmei[tek-1].x=nx;
                zmei[tek-1].y=ny;

                if (zmei[tek-1].x<0)
                    zmei[tek-1].x=Width;
                if (zmei[tek-1].y<0)
                    zmei[tek-1].y=Height;
                if (zmei[tek-1].x>Width)
                    zmei[tek-1].x=0;
                if (zmei[tek-1].y>Height)
                    zmei[tek-1].y=0;
            }
            @Override
            public void run() {
                Canvas canvas;

                while (running) {
                    canvas = null;
                    try {
                        canvas = surfaceHolder.lockCanvas(null);
                        Width=canvas.getWidth();
                        Height=canvas.getHeight();
                        if (canvas == null)
                            continue;
                        upd();
                        kawaii(canvas);
                        SystemClock.sleep(20);

                    } finally {
                        if (canvas != null) {
                            surfaceHolder.unlockCanvasAndPost(canvas);
                        }
                    }
                }
            }
            void kawaii(Canvas canv) {
                canv.drawColor(Color.GREEN);
                for (int i = 0; i < 200; i++){

                    paint.setARGB(255,rand.nextInt(255),rand.nextInt(255),rand.nextInt(255));
                    canv.drawLine(rand.nextInt(Width), rand.nextInt(Height),rand.nextInt(Width), rand.nextInt(Height), paint);
                }

                for(int i=0;i<zmei.length;i++)
                {

                    canv.drawRect((float)zmei[i].x,(float)zmei[i].y,(float)zmei[i].x+ssize,(float)zmei[i].y+ssize,paint);
                }
            }


        }

    }

}